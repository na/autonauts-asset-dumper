// Copyright (c) 2017 Na, released under the 0BSD license
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UERandom = UnityEngine.Random;


public static class AssetImageDumper
{

    static ObjectType[] blacklist = {
        /* Have extra state */
        //ObjectType.StorageGeneric,
        //ObjectType.ConverterFoundation,
        //ObjectType.TreePine,
        //ObjectType.TreeApple,
        //ObjectType.CropWheat,
        //ObjectType.Grass,
        //ObjectType.Native,
        /* Broken/Unimplemented/Uncooperative */
        ObjectType.AnimalChicken,
        ObjectType.LoveHeart,
        ObjectType.MusicalNote,
        //ObjectType.AnimalSheep,
        /* Incorporeal */
        ObjectType.Water,
        ObjectType.Milk,
        ObjectType.Plot,
        ObjectType.Total,
        ObjectType.DigShadow,
        ObjectType.TestParticle
    };

    // Needs a hook set in `MapManager.Load`. Checks for `-dumpassets` commandline argument
    //  TODO: reduce the amount of map initialization asset dumping depends on to speed things up
    /// <summary>A unity coroutine-wotsit to dump all the things.</summary>
    /// <remarks>
    ///     Needs a hook set somewhere in order to run. For example:
    ///     <see cref="MapManager.Load"/> is a good place to add something like this:
    ///     <code>
    ///         if (Environment.GetCommandLineArgs().Contains("-dumpassets"))
    ///         {
    ///             Console.WriteLine("Dumping assets!");
    ///             StartCoroutine(AssetImageDumper.dumpAllTheThings(true));
    ///         }
    ///     </code>
    /// </remarks>
    /// <param name="cmdArg">Wait for first frame and exit on completion if <c>true</c></param>
    public static IEnumerator<YieldInstruction> dumpAllTheThings(bool cmdArg = false)
    {
        // Stuff doesn't render in undiscovered plots!
        foreach (Plot p in PlotManager.Instance.m_Plots) p.SetVisible(true);
        if (cmdArg)
            yield return new WaitForEndOfFrame();

        foreach (var mat in Resources.FindObjectsOfTypeAll<Material>())
        {
            mat.EnableKeyword("_SPECULARHIGHLIGHTS_OFF");
            mat.SetFloat("_SpecularHighlights", 0f);
        }

        // Comment-based testmode :P
        //ObjectType[] renderObjects = { ObjectType.AnimalCow, ObjectType.AnimalSheep };
        ObjectType[] renderObjects = (ObjectType[])Enum.GetValues(typeof(ObjectType));
        //var renderObjects = converters;
        foreach (ObjectType objType in renderObjects)
        {
            if (blacklist.Contains(objType)) continue;
            Console.WriteLine("Photographing: " + objType);
            foreach (var chain in sceneConfigurations)
            {
                yield return MasterManager.Instance.StartCoroutine(dumpImage(objType, chain.configChain, chain.name));
                yield return new WaitForEndOfFrame();
            }
        }
        if (cmdArg)
        {
            Application.Quit();
        }
    }

    // TODO: Make this method less monolithic
    static IEnumerator<YieldInstruction> dumpImage(ObjectType objType, sceneAdjuster[] sceneTweakers, string processName, float fixupDelayTime = 0.6f)
    {
        GameObject go = null;
        try
        {
            // We create the object on a tile, and then move it somewhere else
            //  so that objects that care about their tile don't cause trouble
            go = ObjectTypeList.Instance.CreateObjectFromIdentifier(objType, Vector3.one, Quaternion.identity).gameObject;
        }
        catch (Exception e)
        {
            Debug.Log("Error when dumping " + objType + "\n" + e);
            yield break;
        }
        go.transform.rotation = Quaternion.identity;

        Camera renderCamera = null;
        RenderTexture lastActive;// = RenderTexture.active;
        var targetTexture = RenderTexture.GetTemporary(texSize, texSize, 16, RenderTextureFormat.ARGB32);
        var CameraObj = new GameObject("CameraObj", typeof(Camera));
        renderCamera = CameraObj.GetComponent<Camera>();
        renderCamera.cullingMask = 1 << renderLayer;
        renderCamera.clearFlags = CameraClearFlags.SolidColor;
        renderCamera.targetTexture = targetTexture;
        renderCamera.transform.rotation = cameraRotation;
        renderCamera.transform.position = spawnPosition + renderCamera.transform.forward * -100;

        var texture = new Texture2D(texSize, texSize, TextureFormat.ARGB32, true);

        bool appliedFixup = false;
        try
        {
            fixups[objType](renderCamera, go);
            Console.WriteLine("Applying fixup for: " + objType);
            appliedFixup = true;
        }
        catch (KeyNotFoundException) { } // No-op
        if (appliedFixup) yield return new WaitForSeconds(fixupDelayTime);

        go.transform.position = spawnPosition;
        InstantiationManager.Instance.SetLayer(go, (Layers)renderLayer);

        foreach (var sceneFunc in sceneTweakers)
            sceneFunc(renderCamera, go);
        lastActive = RenderTexture.active;
        RenderTexture.active = targetTexture;
        renderCamera.Render();
        texture.ReadPixels(new Rect(0, 0, texture.width, texture.height), 0, 0);
        texture.Apply();

        // TODO: Find some way to make Debug.Log *not* spew all over the output if an object
        //      doesn't have a human-readable name
        string objectName = ObjectTypeList.Instance.GetHumanReadableNameFromIdentifier(objType) ?? objType.ToString();
        if (objectName == "Autonaut") objectName += String.Format(" v{0}", SaveLoadManager.Instance.m_Version); // HACK
        string objectClass = string.Join("/", getSuperclassesUpTo(go.GetComponent<BaseClass>(), typeof(BaseClass)).ToArray());
        // TODO: Make save location dynamic/cross platform compatible
        var filepath = string.Format("/tmp/autonauts-dump/{0}-{1}/{2}/{3}/{4}.png",
                            SaveLoadManager.Instance.m_Version,SaveLoadManager.Instance.m_VersionName,
                            processName, objectClass, objectName);
        var assetImage = texture.EncodeToPNG();

        var parent = Directory.GetParent(filepath);
        Directory.CreateDirectory(parent.FullName);
        using (var ImageFile = File.OpenWrite(filepath))
        {
            (new BinaryWriter(ImageFile)).Write(assetImage);
        }

        RenderTexture.active = lastActive;

        if (renderCamera)
        {
            renderCamera.targetTexture.Release();
            UnityEngine.Object.Destroy(renderCamera);
        }
        if (go)
        {
            go.transform.position = new Vector3(50, 50, 10);
            // NB: We use BaseClass.StopUsing instead of Object.Destroy
            //  so that objects get correctly cleaned up (e.g. Cows)
            go.GetComponent<BaseClass>().StopUsing();
        }
    }

    static void dumpProps(object thingy)
    {
        Console.WriteLine("Type: {0}", thingy.GetType().Name);
        foreach (PropertyInfo prop in thingy.GetType().GetProperties())
        {
            var propname = prop.Name;
            var propval = prop.GetValue(thingy, null);
            Console.WriteLine("\t{0}: {1}", propname, propval);
        }
    }


    // For positioning/scaling the objects & camera
    delegate void sceneAdjuster(Camera c, GameObject g);

    struct dumpConfiguration
    {
        public string name;
        public sceneAdjuster[] configChain;
        public dumpConfiguration(string name, sceneAdjuster[] configChain)
        {
            this.name = name;
            this.configChain = configChain;
        }
    }

    // The different configurations to export images with
    static readonly dumpConfiguration[] sceneConfigurations = {
        new dumpConfiguration("iso-autoscaled", new sceneAdjuster[] {ortho, genOrthoSize(10), smartScale}),
    };

    static readonly Quaternion cameraRotation = Quaternion.Euler(35.264f, -45, 0); //True Isometric
    //static readonly Quaternion cameraRotation = Quaternion.Euler(90, 0, 0);
    static readonly Vector3 spawnPosition = new Vector3(30, 0, -40);

    const int renderLayer = 25;
    const int texSize = 300; // Pixels
    const int margin = 2; // Pixels
    //NB: Camera.orthographicSize is the "radius", of the height of the viewport
    const float marginScale = margin / (texSize / 2f); // Percent

    // Sketchy reflective extension method to let me call any methods
    static void call(this BaseClass obj, string methodName, params object[] args)
    {
        obj.GetType().GetMethod(methodName,
                                BindingFlags.Public
                                | BindingFlags.NonPublic
                                | BindingFlags.Instance
                                | BindingFlags.Static)
                  .Invoke(obj, args);
    }

    // Sketchy reflective extension method to let me set any field
    static void set(this BaseClass obj, string fieldName, object val)
    {
        obj.GetType().GetField(fieldName,
                               BindingFlags.Public
                               | BindingFlags.NonPublic
                               | BindingFlags.Instance
                               | BindingFlags.Static)
           .SetValue(obj, val);
    }

    static readonly Dictionary<ObjectType, sceneAdjuster> fixups = new Dictionary<ObjectType, sceneAdjuster>
    {
        // Rotations for improved visibility
        {ObjectType.Apple, rotate(0,90,0)}, // Don't look at the apple's stem end-on
        {ObjectType.MetalSawBlade, rotate(0,-90,0)}, // Don't look at the blade edge-on
        {ObjectType.Seedling, rotate(0,0,90)}, // Stand the seedling upright
        {ObjectType.CrudeCog, rotate(-10,0,-10)}, // Don't look at the gear edge-on
        {ObjectType.Beacon, rotate(0,20,0)}, // Prevent the "Inverted cube" effect
        {ObjectType.HatMouseEars, rotate(0,-35,0)}, // Improve ear visibility
        {ObjectType.Weed, rotate(0, 45, 0)},
        {ObjectType.Guiro, rotate(0, 45, 0)},
        // Statey objects
        {ObjectType.StorageGeneric, (_,go) => go.GetComponent<StorageGeneric>().call("SetObjectType", ObjectType.Total)},
        {ObjectType.ConverterFoundation, (_,go) => go.GetComponent<ConverterFoundation>().SetNewBuilding(ObjectType.WorkerAssembler)},
        {ObjectType.TreePine, (_,go) => go.GetComponent<TreePine>().SetFullyGrown()},
        {ObjectType.TreeApple, (_,go) => go.GetComponent<TreeApple>().SetFullyGrown()},
        {ObjectType.CropWheat, (_,go) => {
                var wheat = go.GetComponent<CropWheat>();
                wheat.set("m_Yield", 8);
                wheat.SetGrown();}},
        {ObjectType.Grass, (_,go) => go.GetComponent<Grass>().SetGrown()},
        {ObjectType.Native, (_,go) => go.GetComponent<Native>().SetPose(3)},
        {ObjectType.AnimalCow, (_,go) => {
                var cow = go.GetComponent<AnimalCow>();
                cow.m_FatCount = 25;
                cow.call("UpdateFatCount");
                cow.call("UpdateFatWobble");}},
        {ObjectType.AnimalSheep, (_,go) => {
                var sheep = go.GetComponent<AnimalSheep>();
                sheep.m_FatCount = 25;
                sheep.call("UpdateFatCount");
                sheep.call("UpdateFatWobble");}}
    };


    #region Scene adjusters
    //And sceneadjuster generators

    static sceneAdjuster rotate(float x, float y, float z) => rotate(new Vector3(x, y, z));
    static sceneAdjuster rotate(Vector3 angles) => (
        (_, g) => g.transform.Rotate(angles)
    );

    static void ortho(Camera c, GameObject _)
    {
        c.orthographic = true;
    }

    static sceneAdjuster genOrthoSize(float scale)
    {
        return (Camera renderCamera, GameObject _) =>
        {
            renderCamera.orthographicSize = scale;
        };
    }

    static private Rect rectFromMinMax(Vector2 min, Vector2 max)
    {
        return new Rect(min, max - min);
    }

    static void smartScale(Camera renderCamera, GameObject go)
    {
        Rect objectBounds = getVisibleMeshBounds(renderCamera, go);

        Vector3 offset = (new Vector3(0.5f, 0.5f, 0) - (Vector3)objectBounds.center) * renderCamera.orthographicSize * 2f;
        go.transform.Translate(offset, renderCamera.transform);

        float scaleFactor = Math.Max(objectBounds.width, objectBounds.height);

        renderCamera.orthographicSize *= scaleFactor * (1 + marginScale);
    }
    #endregion

    #region Helpers
    static Rect getVisibleMeshBounds(Camera renderCamera, GameObject go, bool showDebug = false)
    {
        Vector2 min = new Vector3(float.PositiveInfinity, float.PositiveInfinity);
        Vector2 max = new Vector3(float.NegativeInfinity, float.NegativeInfinity);
        foreach (MeshFilter mf in go.GetComponentsInChildren<MeshFilter>())
        {
            var mesh = mf.mesh;
            foreach (Vector3 vert in mesh.vertices)
            {
                var tweakedVert = vert;

                if (mf.name == "BuildingAccessPoint(Clone)"
                   || mf.name == "BuildingSpawnPoint(Clone)")
                {
                    // We rescale the fake verts juust a litle, otherwise the
                    //  renderer nibbles the corners off of the converters' pads
                    float fudgeFactor = -0.01f;
                    tweakedVert.x = (0.75f * vert.x - 0.25f * 5) * 1 + fudgeFactor;
                    tweakedVert.z *= 0.75f + fudgeFactor;
                    if (showDebug)
                    {
                        var dbgv = addDbgPrimitive(PrimitiveType.Sphere,
                                                   Vector3.one * 0.05f, Vector3.zero, Quaternion.identity,
                                                   Color.Lerp(new Color(0, 1, 1, 0.5f), new Color(1, 0, 1, 0.5f), (tweakedVert.z + 5) / 10));
                        dbgv.transform.SetParent(mf.transform);
                        dbgv.transform.localPosition = tweakedVert;
                        dbgv.transform.Translate(renderCamera.transform.forward * -50);
                    }
                }
                var worldVert = mf.transform.TransformPoint(tweakedVert);
                var cameraVert = renderCamera.WorldToViewportPoint(worldVert);
                min = Vector2.Min(min, cameraVert);
                max = Vector2.Max(max, cameraVert);

            }
        }
        return Rect.MinMaxRect(min.x, min.y, max.x, max.y);
    }

    static List<string> getSuperclassesUpTo(BaseClass startObj, Type rootClass)
    {
        var typeParents = new List<string>();

        Type startType = startObj.GetType();
        if (!startType.IsSubclassOf(rootClass)) return typeParents;

        Type parent = startType;
        do
        {
            parent = parent.BaseType;
            if (parent == null || parent == rootClass || parent == typeof(object))
            {
                break;
            }
            typeParents.Add(parent.Name);
        } while (parent != rootClass);

        typeParents.Reverse();
        return typeParents;
    }
    #endregion

    #region debugging bits

    static void printObjTree(GameObject go, int level = 0)
    {
        Console.WriteLine("{0}{1}", "".PadLeft(level, '\t'), go.name);
        foreach (Component c in go.GetComponents<Component>())
        {
            Console.WriteLine("{0} C:{1}", "".PadLeft(level, '\t'), c);
        }
        foreach (Transform t in go.transform)
        {
            printObjTree(t.gameObject, level + 1);
        }
    }

    static void showMeshPoints(MeshFilter mf)
    {
        foreach (Vector3 vert in mf.mesh.vertices)
        {
            var randomColor = Color.HSVToRGB(UERandom.value, 1, 1);
            randomColor.a = 0.5f;
            var debugPoint = addDbgPrimitive(PrimitiveType.Sphere, Vector3.one * 0.1f,
                                             vert, Quaternion.identity, randomColor);
        }
    }

    static GameObject drawBoundsCube(Bounds bnds, Color matColor) =>
        addDbgPrimitive(PrimitiveType.Cube, bnds.size, bnds.center,
                       Quaternion.identity, matColor);

    static GameObject drawViewportQuad(Rect screenRect, Camera renderCamera, Color matColor, float depth = 0)
    {
        var scale = new Vector3(screenRect.width, screenRect.height, 0)
                 * renderCamera.orthographicSize * 2f
                 + Vector3.forward;
        var position = renderCamera.ViewportToWorldPoint(new Vector3(screenRect.center.x, screenRect.center.y, depth));
        return addDbgPrimitive(PrimitiveType.Quad, scale, position,
                              renderCamera.transform.rotation, matColor);
    }

    static GameObject addDbgPrimitive(PrimitiveType kind, Vector3 scale, Vector3 position,
                                     Quaternion rotation, Color matColor)
    {
        var debugObj = GameObject.CreatePrimitive(kind);
        StandardShaderUtils.MakeObjectTransparent(debugObj);
        debugObj.GetComponent<Renderer>().material.color = matColor;

        debugObj.transform.localScale = scale;
        debugObj.transform.position = position;
        debugObj.transform.rotation = rotation;

        UnityEngine.Object.Destroy(debugObj);
        return debugObj;
    }
    #endregion
}